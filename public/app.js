document.querySelector('#hello').addEventListener('click', () => {
    fetch('/hello').then(res => res.json()).then(data => console.log(data))
})

document.querySelector('#world').addEventListener('click', () => {
    fetch('/world').then(res => res.json()).then(data => console.log(data))
})