# Mqtt client Express

## Работа с соединением к брокеру 
### Установка сервера для создания брокера
```
apt install mosquitto
```
### Запуск сервера
```
mosquitto
```
Стандартный порт - 1883

# Работа с отправкой и получение сообщений mqtt
## Необходимо глобально установить mqtt
```
npm install -g mqtt
```
Для того, чтобы можно было подписаться без написания отдельного кода

#### Отправка сообщений
```
mqtt pub -t "test" -h "localhost" -m "Hello!"
```

#### Подписка на "топик"
```
mqtt sub -t "test" -h "localhost"
```

### Не забываем для  ``` npm install ```

## файл express.js 
``` js
5:  const mqtt = require('mqtt')
6:  // Задавание соединения - порт указывать не нужно, он стандартный - 1883 
7:  // mqtt://localhost - запуск сервера на своем компе с помощью mosquitto (linux or mac) 
8:  // Либо использование стандaртных брокеров для mqtt
9:  const client = mqtt.connect('mqtt://localhost')
10: 
11: client.on('connect', () => {
12:     console.log('Done!');
13: })

```

## Запуск сервера
```
npm run dev
```