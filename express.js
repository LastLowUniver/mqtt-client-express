const express = require('express')
const {Router} = require('express')
const path = require('path')

const mqtt = require('mqtt')
// Задавание соединения: 
// localhost - запуск сервера на своем компе с помощью mosquitto (linux or mac)
// Либо использование стандортных брокеров для mqtt
const client = mqtt.connect('mqtt://localhost')

client.on('connect', () => {
    console.log('Done!');
})

const app = express()
const router = Router()

app.use(express.static(path.join(__dirname, 'public')))

router.get('/', (req, res) => {
    res.render('index.html')
})

router.get('/hello', (req, res) => {
    client.publish('test', '#Hello')
    res.status(200).send({message: "Success sended"})
})

router.get('/world', (req, res) => {
    client.publish('test', '#World')
    res.status(200).send({message: "Success sended"})
})

app.use('/', router)

const PORT = process.env.PORT || 3000

async function start() {
    try {
        app.listen(PORT, () => {
            console.log(`Server port ${PORT}`)
        })
    } catch(e) {
        console.log(e)
    }
}

start()