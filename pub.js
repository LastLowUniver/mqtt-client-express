const mqtt = require('./node_modules/mqtt/mqtt.js')  // require mqtt
const client = mqtt.connect('mqtt://localhost')

client.on('connect', () => {
    console.log('Done!');
})

document.querySelector('#hello').addEventListener('click', () => {
    client.publish('test', 'Hello!')
})

document.querySelector('#world').addEventListener('click', () => {
    client.publish('test', 'World!')
})